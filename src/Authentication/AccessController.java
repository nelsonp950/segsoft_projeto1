package Authentication;

import Exceptions.AccessControlErrorException;
import Utils.Utils;

import javax.servlet.http.HttpServletRequest;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccessController implements AccessControllerInterface{
    private static final String dbConnecitonURL = "jdbc:mysql://localhost:3306/authentication";
    public static final String DIVISOR = "-";
    Connection connection= null;
    Statement stmt = null;


    private Connection dbConnection () throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return DriverManager.getConnection(dbConnecitonURL,"root","");
    }

    private ResultSet getResultFromQuery (String query) throws SQLException {


        //Database connection
        connection = dbConnection();

        //create statement
        stmt = connection.createStatement();

        //Set query
        String sqlQryStr = query;

        //stmt.executeUpdate(sqlQryStr);

        //Execute query
        ResultSet rslt = stmt.executeQuery(sqlQryStr); // Execute query


        return rslt;
    }


    private void updateDatabase (String query) throws SQLException {
        Connection connection= null;
        Statement stmt = null;

        //Database connection
        connection = dbConnection();

        //create statement
        stmt = connection.createStatement();

        //Set query
        String sqlQryStr = query;

        stmt.executeUpdate(sqlQryStr);

        if (stmt != null) stmt.close();
        if (connection != null) connection.close();

    }

    private void killDBconnection() throws SQLException {
        if (stmt != null) stmt.close();
        if (connection != null) connection.close();
    }


    public AccessController() {

    }

    @Override
    public void makeKey(String user,String resource, String operation, String ExpireTime) {
    //Simplefied version
        String rawCapability =  user + DIVISOR + resource + DIVISOR +  operation + DIVISOR + ExpireTime;
        String capability = null;
        try {
            capability = Utils.encrypt(rawCapability);

            updateDatabase("INSERT INTO capability values ( '" + user + "','" + capability + "');");
            killDBconnection();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Assuming only two capabilities available
    @Override
    public List<String> getKey(HttpServletRequest req) {
        String username = req.getParameter("name");
        List<String> list = new ArrayList();

        try {
            ResultSet rslt = getResultFromQuery("SELECT * FROM capability WHERE username = " + "'" + username + "';");
            rslt.next();
            list.add(rslt.getString("capability"));
            rslt.next();
            list.add(rslt.getString("capability"));
            killDBconnection();
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Simple implementation for read and write scenario
    @Override
    public boolean checkPermission(String user, List<String> capabilities, String resource, String operation) throws AccessControlErrorException {


        try {
            String[] attributes =  Utils.decrypt(capabilities.get(0)).split(DIVISOR);
            String capUser= attributes[0];
            String capResource = attributes[1];
            String capOperation = attributes[2];
            String capExpireTime = attributes[3];//Not implemented
            if(capUser.equals(user) && capResource.equals(resource) && capOperation.equals(operation))
                return true;
             attributes =  Utils.decrypt(capabilities.get(1)).split(DIVISOR);
             capUser= attributes[0];
             capResource = attributes[1];
             capOperation = attributes[2];
             capExpireTime = attributes[3];//Not implemented
            if(capUser.equals(user) && capResource.equals(resource) && capOperation.equals(operation))
                return true;
            else
                 throw new AccessControlErrorException("No access!");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
