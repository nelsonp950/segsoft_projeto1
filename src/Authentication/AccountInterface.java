package Authentication;

/**
 * Created by nelson on 09/11/2017.
 */
public interface AccountInterface {


     String getAccountname() ;

     String getPassword() ;



     boolean isLogged_in() ;

     boolean isLocked() ;

     /*void setAccountname(String accountname);

     void setLogged_in(boolean logged_in);

   void setPassword(String password) ;

     void setLocked(boolean locked) ;*/

}
