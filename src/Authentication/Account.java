package Authentication;

/**
 * Created by nelson on 09/11/2017.
 */
public class Account implements AccountInterface{

    String accountname;
    String password;
    boolean logged_in;
    boolean locked;


    public Account(String accountname, String password, boolean loggedIn, boolean lockedIn) {
        this.accountname = accountname;
        this.password = password;
        this.locked = lockedIn;
        this.logged_in = loggedIn;
    }

    public String getAccountname() {
        return accountname;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public boolean isLogged_in() {
        return logged_in;
    }

    @Override
    public boolean isLocked() {
        return locked;
    }


   /* @Override
    public void setLocked(boolean locked) {
        this.locked = locked;
    }
    @Override
    public void setLogged_in(boolean logged_in) {
        this.logged_in = logged_in;
    }
    @Override
    public void setPassword(String password) {
        this.password = password;
    }
    @Override
    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }*/
}
