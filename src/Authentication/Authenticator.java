package Authentication; /**
 * Created by nelson on 09/11/2017.
 */

import Exceptions.AuthenticationErrorException;
import Exceptions.LockedAccountException;
import Exceptions.UndefinedAccountException;
import Exceptions.UserAccountCreationException;
import Utils.Utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.*;


public class Authenticator implements AuthenticatorInterface{
    private static final String dbConnecitonURL = "jdbc:mysql://localhost:3306/authentication";

    AccessControllerInterface access;

    public Authenticator() {

        access = new AccessController();
    }


    Connection connection= null;
    Statement stmt = null;

    private Connection dbConnection () throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return DriverManager.getConnection(dbConnecitonURL,"root","");
    }

    private ResultSet getResultFromQuery (String query) throws SQLException {


        //Database connection
        connection = dbConnection();

        //create statement
        stmt = connection.createStatement();

        //Set query
        String sqlQryStr = query;

        //stmt.executeUpdate(sqlQryStr);

        //Execute query
        ResultSet rslt = stmt.executeQuery(sqlQryStr); // Execute query


        return rslt;
    }


    private void updateDatabase (String query) throws SQLException {
        Connection connection= null;
        Statement stmt = null;

        //Database connection
        connection = dbConnection();

        //create statement
        stmt = connection.createStatement();

        //Set query
        String sqlQryStr = query;

        stmt.executeUpdate(sqlQryStr);

        if (stmt != null) stmt.close();
        if (connection != null) connection.close();

    }

    private void killDBconnection() throws SQLException {
        if (stmt != null) stmt.close();
        if (connection != null) connection.close();
    }

    @Override
    public void create_account(String requester, String name, String pwd1, String pwd2) throws UserAccountCreationException {


        // Preconditions:
        // ---------------
        //1- Equal passwords
        //2- New user
        //3- Root account

        //1
        if(!pwd1.equals(pwd2))
            throw new UserAccountCreationException("Passwords do no match");


        try {

            //2
            ResultSet rslt = getResultFromQuery("SELECT username FROM logins WHERE username = "+ "'" + name + "';");

            if (rslt.isBeforeFirst()) {
                throw new UserAccountCreationException("User already exists");
            }

            //3
            if (!requester.equals("root")){
                throw new UserAccountCreationException("Your account does not have the permisions");
            }




        // Action: Create new account
        // - Password is encrypted

            updateDatabase("INSERT INTO logins values ( '" + name + "','" + Utils.encrypt(pwd1) + "',0,0);");
            killDBconnection();
            //Simplified version: Always give full access
            access.makeKey(name, "full","edit","0");
            access.makeKey(name, "full","write","0");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void delete_account(String requester, String name) throws UndefinedAccountException, LockedAccountException {
        // Preconditions:
        // ---------------
        //1- Account exists
        //2- Account not logged in
        //3- Account is locked
        //4- Is root

        try {
            ResultSet rslt = getResultFromQuery("SELECT * FROM logins WHERE username = " + "'" + name + "';");

            //1
            if (!rslt.isBeforeFirst()) {
                throw new UndefinedAccountException("User not found");
            }
            rslt.next();
            //2
            int isLoggedin = rslt.getInt("isLoggedin");
            if (isLoggedin==1) {
                throw new LockedAccountException("Account is logged in");
            }

            //3
            int isLocked = rslt.getInt("isLocked");
            if (isLocked==0) {
                throw new LockedAccountException("Account not locked");
            }

            //4
            //3
            if (!requester.equals("root")){
                throw new UndefinedAccountException("Your account does not have the permisions");
            }

            killDBconnection();
            //Action delete account
            updateDatabase("delete FROM logins WHERE username = "+ "'" + name + "';");


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setAccountLock(String name, boolean lock) throws UndefinedAccountException, LockedAccountException {
        // Preconditions:
        // ---------------
        //1- Account exists
        //2- Account not logged in

        try {
            ResultSet rslt = getResultFromQuery("SELECT * FROM logins WHERE username = " + "'" + name + "';");

            //1
            if (!rslt.isBeforeFirst()) {
                throw new UndefinedAccountException("User not found");
            }
            rslt.next();
            //2
            int isLoggedin = rslt.getInt("isLoggedin");
            if (isLoggedin==1) {
                throw new LockedAccountException("Account is logged in");
            }


            killDBconnection();
            //Action delete account
            updateDatabase("UPDATE logins set isLocked = " + (lock?1:0) + " WHERE username = '" + name + "';");


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Account get_account(String name) throws UndefinedAccountException {
            // Preconditions:
            // ---------------
            //1- Account exists

            try {
                ResultSet rslt = getResultFromQuery("SELECT * FROM logins WHERE username = " + "'" + name + "';");

                //1
                if (!rslt.isBeforeFirst()) {
                    throw new UndefinedAccountException("User not found");
                }

               //Action: return account
                rslt.next();
                Account account = new Account(rslt.getString("username"), rslt.getString("password"),rslt.getBoolean("isLoggedIn"),rslt.getBoolean("isLocked"));

                killDBconnection();
                return account;
            } catch (SQLException e) {
                e.printStackTrace();
            }


        return null;
    }
    @Override
    public void change_pwd(String name, String pwd1, String pwd2) throws UndefinedAccountException, UserAccountCreationException {

        // Preconditions:
        // ---------------
        //1- Account exists
        //2- Password match

        try {
            ResultSet rslt = getResultFromQuery("SELECT * FROM logins WHERE username = " + "'" + name + "';");

            //1
            if (!rslt.isBeforeFirst()) {
                throw new UndefinedAccountException("User not found");
            }


            //2
            if (!pwd1.equals(pwd2))
                throw new UserAccountCreationException("Passwords do no match");

            killDBconnection();
            // Action: Update password
            // - Password is encrypted

            updateDatabase("UPDATE logins set password = '" + Utils.encrypt(pwd1) + "' WHERE username = '" + name + "';");

        } catch (SQLException e) {
            e.printStackTrace();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
        @Override
    public Account login(String name, String pwd) throws AuthenticationErrorException {

        // Preconditions:
        // ---------------
        //1- Account exists
        //2- Account is locked
        //3- Compare password with stored hash


            ResultSet rslt = null;
            try {
                rslt = getResultFromQuery("SELECT * FROM logins WHERE username = " + "'" + name + "';");

            //1
            if (!rslt.isBeforeFirst()) {
                throw new AuthenticationErrorException("User not found");
            }
            rslt.next();
            //2
            int isLocked = rslt.getInt("isLocked");
            if (isLocked==1) {
                throw new AuthenticationErrorException("Account is locked");
            }

            //3
            String storedPw = rslt.getString("password");
            if(!(storedPw.equals(Utils.encrypt(pwd))))
                throw new AuthenticationErrorException("Passwords incorrect");

            killDBconnection();

            //Action:
            //- Set has logged in
            //-return account

            updateDatabase("UPDATE logins set isLoggedin = '1' WHERE username = '" + name + "';");


                Account acc = get_account(name);
                return acc;
            } catch (SQLException e) {
               throw new AuthenticationErrorException("Erro sql " + e.getErrorCode());
            } catch (Exception e) {
                e.printStackTrace();
            }









        return null;
    }
    @Override
    public void logout(Account acc) throws AuthenticationErrorException {

        // Preconditions:
        // ---------------
        //1- Account exists

        try {
            ResultSet rslt = getResultFromQuery("SELECT * FROM logins WHERE username = " + "'" + acc.getAccountname() + "';");

            //1
            if (!rslt.isBeforeFirst()) {
                throw new AuthenticationErrorException("User not found");
            }
            killDBconnection();
            //Action
            //-Logout


            updateDatabase("UPDATE logins set isLoggedin = '0' WHERE username = '" + acc.getAccountname() + "';");

        } catch (SQLException e) {
            e.printStackTrace();


        }

    }
    @Override
    public Account login(HttpServletRequest req, HttpServletResponse resp) throws AuthenticationErrorException, UndefinedAccountException {

        HttpSession session  = req.getSession(false);

        String username = (String) session.getAttribute("username");

        Account account = get_account(username);

        if (account.logged_in)
            return account;
        else
        throw new AuthenticationErrorException("Account session check failed");

    }

    @Override
    public void logAction(String operation, String username) {
        try {
            updateDatabase("INSERT INTO logger values (null,'" + username + "','" + operation + "');");

            killDBconnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
