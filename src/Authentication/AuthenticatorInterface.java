package Authentication;

import Exceptions.AuthenticationErrorException;
import Exceptions.LockedAccountException;
import Exceptions.UndefinedAccountException;
import Exceptions.UserAccountCreationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by nelson on 09/11/2017.
 */
public interface AuthenticatorInterface {

    void create_account(String requester, String name, String pwd1, String pwd2) throws UserAccountCreationException;

    void delete_account(String requester, String name) throws UndefinedAccountException, LockedAccountException;

    void setAccountLock(String name,boolean lock) throws UndefinedAccountException, LockedAccountException;

    Account get_account(String name) throws UndefinedAccountException;

    void change_pwd(String name, String pwd1, String pwd2) throws UndefinedAccountException, UserAccountCreationException;

    Account login(String name, String pwd) throws AuthenticationErrorException;

    void logout(Account acc) throws AuthenticationErrorException;

    Account login(HttpServletRequest req, HttpServletResponse resp) throws AuthenticationErrorException, UndefinedAccountException;

    void logAction(String operation, String username);
}
