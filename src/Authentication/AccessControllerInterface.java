package Authentication;


import Exceptions.AccessControlErrorException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface AccessControllerInterface {


    void makeKey( String user,String resource, String operation, String ExpireTime);

    List<String> getKey(HttpServletRequest req);

    boolean checkPermission( String user,List<String> capabilities, String resource, String operation) throws AccessControlErrorException;
}
