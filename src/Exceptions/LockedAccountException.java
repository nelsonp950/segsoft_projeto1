package Exceptions;

/**
 * Created by NelsonTORRE on 13/11/2017.
 */
public class LockedAccountException extends Exception {


    private static final long serialVersionUID = 1L;

    public LockedAccountException () {
        super();
    }


    public LockedAccountException (String message){
        super(message);
    }


}
