package Exceptions;

/**
 * Created by NelsonTORRE on 13/11/2017.
 */
public class UserAccountCreationException extends Exception {

    private static final long serialVersionUID = 1L;

    public UserAccountCreationException() {
        super();
    }


    public UserAccountCreationException(String message){
        super(message);
    }
}
