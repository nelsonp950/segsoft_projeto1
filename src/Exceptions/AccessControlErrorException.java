package Exceptions;

public class AccessControlErrorException extends Throwable {

    private static final long serialVersionUID = 1L;

    public AccessControlErrorException () {
        super();
    }


    public AccessControlErrorException (String message){
        super(message);
    }


}
