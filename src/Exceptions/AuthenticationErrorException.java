package Exceptions;

/**
 * Created by NelsonTORRE on 13/11/2017.
 */
public class AuthenticationErrorException  extends Exception {


        private static final long serialVersionUID = 1L;

        public AuthenticationErrorException () {
            super();
        }


        public AuthenticationErrorException (String message){
            super(message);
        }


}
