package servlet;

import Authentication.Account;
import Authentication.Authenticator;
import Authentication.AuthenticatorInterface;
import Exceptions.AuthenticationErrorException;
import Exceptions.UndefinedAccountException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/Login")
public class Login extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();





        response.setContentType("text/html");



        response.setContentType("text/html");
        out.println("<html>");
        out.println("<head>");
        out.println("<title> Login </title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h3> Login </h3>");
        out.println("<P>");
        out.println("<form action=Login method=POST>");
        out.println("Username:");
        out.println("<input type=text size=15 name=name>");
        out.println("<br>");
        out.println("Password:");
        out.println("<input type=text size=15 name=password1>");
        out.println("<br>");
        out.println("<br>");
        out.println("<input type=submit value=Login>");
        out.println("</form>");
        out.println("</body>");
        out.println("</html>");

    }



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        String name = request.getParameter("name");
        String pwd1 = request.getParameter("password1");

        HttpSession session = request.getSession(true);
        AuthenticatorInterface authenticator = new Authenticator();



        try {


                Account account = authenticator.login(name, pwd1);
                authenticator.logAction("Login",name);

            if (account!=null) {
                session.setAttribute("username", account.getAccountname());

                response.sendRedirect("Home");

            }
            } catch (AuthenticationErrorException e) {
            out.println("<html>" + e.getMessage() + "</html>");

        }




    }
}
