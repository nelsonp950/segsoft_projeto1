package servlet;

import Authentication.AccessController;
import Authentication.Authenticator;
import Authentication.AuthenticatorInterface;
import Exceptions.AccessControlErrorException;
import Exceptions.AuthenticationErrorException;
import Exceptions.UndefinedAccountException;
import Exceptions.UserAccountCreationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


@WebServlet("/Home")
public class Home extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        AuthenticatorInterface authenticator = new Authenticator();

        try {
            String username = authenticator.login(request, response).getAccountname();
            AccessController access = new AccessController();
            List<String> caps = access.getKey(request);
            access.checkPermission(username,caps,"full","edit");

        response.setContentType("text/html");



        response.setContentType("text/html");

        out.println("<html>");
        out.println("<head>");
        out.println("<title> Actions </title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h3> Actions </h3>");
        out.println("<P>");

        if (username.equals("root")) {
            out.println("<form action=CreateUser method=GET>");
            out.println("<input type=submit value=\"Create Account\">");
            out.println("</form>");

            out.println("<form action=DeleteUser method=GET>");
            out.println("<input type=submit value=\"Delete Account\">");
            out.println("</form>");
        }

        out.println("<form action=ChangePassword method=GET>");
        out.println("<input type=submit value=\"Change Password\">");
        out.println("</form>");

        out.println("<form action=LockAccount method=GET>");
        out.println("<input type=submit value=\"Block Account\">");
        out.println("</form>");

        out.println("<form action=UnlockAccount method=GET>");
        out.println("<input type=submit value=\"Unblock Account\">");
        out.println("</form>");

        out.println("<form action=Logout method=GET>");
        out.println("<input type=submit value=\"Logout\">");
        out.println("</form>");


        out.println("</body>");
        out.println("</html>");

        } catch (AuthenticationErrorException e) {
            out.println("<html> Error: " +e.getMessage()+"</html>");
        } catch (UndefinedAccountException e) {
            out.println("<html> Error: " +e.getMessage()+"</html>");
        } catch (AccessControlErrorException e) {
            out.println("<html> Error: " +e.getMessage()+"</html>");
        }
    }



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



    }
}
