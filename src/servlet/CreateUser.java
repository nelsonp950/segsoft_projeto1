package servlet;

import Authentication.AccessController;
import Authentication.Authenticator;
import Authentication.AuthenticatorInterface;
import Exceptions.AccessControlErrorException;
import Exceptions.AuthenticationErrorException;
import Exceptions.UndefinedAccountException;
import Exceptions.UserAccountCreationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by NelsonTORRE on 13/11/2017.
 */

@WebServlet("/CreateUser")
public class CreateUser extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        AuthenticatorInterface authenticator = new Authenticator();
        PrintWriter out = response.getWriter();

        String username = null;
        try {
            username = authenticator.login(request, response).getAccountname();
            AccessController access = new AccessController();
            List<String> caps = access.getKey(request);
            access.checkPermission(username,caps,"full","edit");


            response.setContentType("text/html");



            out.println("<html>");
            out.println("<head>");
            out.println("<title> Create Account </title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h3> Create Account </h3>");
            out.println("<P>");
            out.println("<form action=CreateUser method=POST>");
            out.println("Username:");
            out.println("<input type=text size=15 name=name>");
            out.println("<br>");
            out.println("Password:");
            out.println("<input type=text size=15 name=password1>");
            out.println("<br>");
            out.println("Repeat Password:");
            out.println("<input type=text size=15 name=password2>");
            out.println("<br>");
            out.println("<input type=submit value=Create>");
            out.println("</form>");
            out.println("<form action=Home method=GET>");
            out.println("<input type=submit value=Back>");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        } catch (AuthenticationErrorException e) {
            out.println("<html> Error: " +e.getMessage()+"</html>");
        } catch (UndefinedAccountException e) {
            out.println("<html> Error: " +e.getMessage()+"</html>");
        } catch (AccessControlErrorException e) {
            out.println("<html> Error: " +e.getMessage()+"</html>");
        }
    }



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        AuthenticatorInterface authenticator = new Authenticator();
        try {
        HttpSession session = request.getSession(false);
        String requester = (String) session.getAttribute("username");
        authenticator.login(request, response);
        String pwd1 = request.getParameter("password1");
        String pwd2 = request.getParameter("password2");
        String username = request.getParameter("name");


            authenticator.create_account(requester,username,pwd1, pwd2);
            authenticator.logAction("Create User",username);
            response.sendRedirect("Home");


        } catch (UserAccountCreationException e) {
            out.println("<html> Error: " +e.getMessage()+"</html>");
        } catch (UndefinedAccountException e) {
            out.println("<html> Error: " +e.getMessage()+"</html>");
        } catch (AuthenticationErrorException e) {
            out.println("<html> Error: " +e.getMessage()+"</html>");
        }

    }
}
