package servlet;

import Authentication.Account;
import Authentication.Authenticator;
import Authentication.AuthenticatorInterface;
import Exceptions.AuthenticationErrorException;
import Exceptions.UndefinedAccountException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/Logout")
public class Logout extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession(false);

        AuthenticatorInterface authenticator = new Authenticator();

        String user = (String) session.getAttribute("username");

        session.invalidate();

        try {
            authenticator.logout(authenticator.get_account(user));
            authenticator.logAction("Logout",user);
            response.sendRedirect("Login");
        } catch (AuthenticationErrorException e) {
            out.println("<html>" + e.getMessage() + "</html>");
        } catch (UndefinedAccountException e) {
            out.println("<html>" + e.getMessage() + "</html>");
        }
    }



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {






    }
}
