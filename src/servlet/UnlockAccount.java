package servlet;

import Authentication.AccessController;
import Authentication.Authenticator;
import Authentication.AuthenticatorInterface;
import Exceptions.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


@WebServlet("/UnlockAccount")
public class UnlockAccount extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        AuthenticatorInterface authenticator = new Authenticator();
        PrintWriter out = response.getWriter();
        try {
            String username = authenticator.login(request, response).getAccountname();
            AccessController access = new AccessController();
            List<String> caps = access.getKey(request);
            access.checkPermission(username,caps,"full","edit");

        response.setContentType("text/html");



        response.setContentType("text/html");

        out.println("<html>");
        out.println("<head>");
        out.println("<title> Unlock Account </title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h3> Unlock Account</h3>");
        out.println("<P>");
        out.println("<form action=unlockAccount method=POST>");
        out.println("Username:");
        out.println("<input type=text size=15 name=name>");
        out.println("<br>");
        out.println("<input type=submit value=Block>");
        out.println("</form>");
        out.println("<form action=Home method=GET>");
        out.println("<input type=submit value=Back>");
        out.println("</form>");
        out.println("</body>");
        out.println("</html>");

    } catch (AuthenticationErrorException e) {
            out.println("<html> Error: " +e.getMessage()+"</html>");
    } catch (UndefinedAccountException e) {
            out.println("<html> Error: " +e.getMessage()+"</html>");
    } catch (AccessControlErrorException e) {
            out.println("<html> Error: " +e.getMessage()+"</html>");
        }
    }



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        AuthenticatorInterface authenticator = new Authenticator();
        try {

            authenticator.login(request, response);

            String username = request.getParameter("name");



            authenticator.setAccountLock(username,false);
            authenticator.logAction("UnlockAccount",username);
            response.sendRedirect("Home");


        }  catch (LockedAccountException e) {
            out.println("<html> Error: " +e.getMessage()+"</html>");
        } catch (UndefinedAccountException e) {
            out.println("<html> Error: " +e.getMessage()+"</html>");
        } catch (AuthenticationErrorException e) {
            out.println("<html> Error: " +e.getMessage()+"</html>");
        }

    }
}
