package servlet;

import Authentication.AccessController;
import Authentication.Authenticator;
import Authentication.AuthenticatorInterface;
import Exceptions.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.Principal;
import java.util.List;

/**
 * Created by NelsonTORRE on 13/11/2017.
 */

@WebServlet("/ChangePassword")
public class ChangePassword extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        AuthenticatorInterface authenticator = new Authenticator();
        PrintWriter out = response.getWriter();

        String username = null;
        try {
            username = authenticator.login(request, response).getAccountname();
            AccessController access = new AccessController();
            List<String> caps = access.getKey(request);
            access.checkPermission(username,caps,"full","edit");


            response.setContentType("text/html");

            out.println("<html>");
            out.println("<head>");
            out.println("<title> Change Password </title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h3> Change Password </h3>");
            out.println("<P>");
            out.println("<form action=ChangePassword method=POST>");
            out.println("New password:");
            out.println("<input type=text size=15 name=pwd1>");
            out.println("Confirm password:");
            out.println("<input type=text size=15 name=pwd2>");
            out.println("<br>");
            out.println("<input type=submit value=Proceed>");
            out.println("</form>");
            out.println("<form action=Home method=GET>");
            out.println("<input type=submit value=Back>");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        } catch (AuthenticationErrorException e) {
            out.println("<html>" + e.getMessage() + "</html>");
        } catch (UndefinedAccountException e) {
            out.println("<html>" + e.getMessage() + "</html>");
        } catch (AccessControlErrorException e) {
            out.println("<html>" + e.getMessage() + "</html>");
        }
    }






    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        AuthenticatorInterface authenticator = new Authenticator();
        String username = null;
        try {
            username = authenticator.login(request, response).getAccountname();

            authenticator.logAction("PasswordChange",username);

        String pwd1 = request.getParameter("password1");
        String pwd2 = request.getParameter("password2");



            authenticator.change_pwd(username,pwd1,pwd2);
        } catch (UndefinedAccountException e) {
            out.println("<html>" + e.getMessage() + "</html>");
        }catch (UserAccountCreationException e) {
            out.println("<html>" + e.getMessage() + "</html>");
        } catch (AuthenticationErrorException e) {
            out.println("<html>" + e.getMessage() + "</html>");
        }
    }
}
