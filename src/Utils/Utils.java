package Utils;

/**
 * Created by nelson on 10/11/2017.
 */
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.util.Base64.*;

public class Utils {



    private static final String ALGO = "AES";

    private static final byte[] keyValue =
            new byte[] { 'F', 'C', 'T', '/', 'U', 'N', 'L', 'r',
            'o', 'c', 'k','s', '!', '!', 'd', 'a' };

        static Key key = new SecretKeySpec(keyValue, ALGO);


        public static String encrypt(String Data) throws Exception {
            Cipher c = Cipher.getInstance(ALGO);
            c.init(Cipher.ENCRYPT_MODE, key);
            byte[] encVal = c.doFinal(Data.getBytes());
            return java.util.Base64.getEncoder().encodeToString(encVal);
        }



        public static String decrypt(String encrypted) throws Exception {
                Cipher c = Cipher.getInstance(ALGO);
                c.init(Cipher.DECRYPT_MODE, key);
                byte[] decodedValue = java.util.Base64.getDecoder().decode(encrypted);
                byte[] decValue = c.doFinal(decodedValue);
                return new String(decValue);
        }


}
